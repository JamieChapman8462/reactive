package hello

import rx.Observable

class Main {
    static void main(String[] args) {
        def numbers   = Observable.from([1, 3, 5, 7, 9])
        def listLetters = [2, 4, 6]
        def multiply = {n -> Observable.from(listLetters.collect( { it * n } )) }
        def timesByTwo = { it -> it * 2 }
        int index = 1

        1.upto(1, {
            Output.printOutThisIsAnEvent()
        })


        println "Combine has a class of: ${multiply.class}"

        numbers.map(timesByTwo).subscribe(
                { println("${index}.onNext: ${it}")
                          ++index},
                { println("OnError: " + it.getMessage()) },
                { println("onComplete: Sequence finished") }
        )
    }
}
